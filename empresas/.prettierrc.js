module.exports = {
  bracketSpacing: false,
  jsxBracketSameLine: true,
  singleQuote: true,
  trailingComma: 'all',
  useTabs: true,
  endOfLine:"auto",
  tabWidth: 4,
  format_on_save: true
};
