import React, {useState, useEffect} from 'react';
import 'react-native-gesture-handler';
import {NavigationContainer, DefaultTheme} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import AsyncStorage from '@react-native-community/async-storage';

import Home from './src/components/Home';
import Login from './src/components/Login';
import Enterprises from './src/components/Enterprises';
import EnterpriseDetail from './src/components/EnterpriseDetail';

const Stack = createStackNavigator();

const MyTheme = {
	...DefaultTheme,
	colors: {
		...DefaultTheme.colors,
		primary: 'rgb(255, 45, 85)',
		text: 'rgb(65, 77, 121)',
		background: 'rgb(255, 255, 255)',
	},
};

const App = () => {
	const [accessToken, setAccessToken] = useState(null);
	useEffect(() => {
		const prepare = async () => {
			setAccessToken(await AsyncStorage.getItem('access-token'));
		};
		prepare();
	}, []);
	return (
		<NavigationContainer theme={MyTheme}>
			{accessToken && (
				<Stack.Navigator>
					<Stack.Screen
						name="Enterprises"
						component={Enterprises}
						options={{headerShown: false}}
					/>
					<Stack.Screen
						name="EnterpriseDetail"
						component={EnterpriseDetail}
						options={{
							title: 'Enterprise Detail',
							headerStyle: {
								backgroundColor: '#414d79',
							},
							headerTintColor: '#FFF',
						}}
					/>
					<Stack.Screen
						name="Home"
						component={Home}
						options={{headerShown: false}}
					/>
					<Stack.Screen name="Login" component={Login} />
				</Stack.Navigator>
			)}
			{!accessToken && (
				<Stack.Navigator>
					<Stack.Screen
						name="Home"
						component={Home}
						options={{headerShown: false}}
					/>
					<Stack.Screen
						name="Login"
						component={Login}
						options={{headerShown: false}}
					/>
					<Stack.Screen
						name="Enterprises"
						component={Enterprises}
						options={{headerShown: false}}
					/>
					<Stack.Screen
						name="EnterpriseDetail"
						component={EnterpriseDetail}
						options={{
							title: 'Enterprise Detail',
							headerStyle: {
								backgroundColor: '#414d79',
							},
							headerTintColor: '#FFF',
						}}
					/>
				</Stack.Navigator>
			)}
		</NavigationContainer>
	);
};

export default App;
