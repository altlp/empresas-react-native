import React, {useState, useEffect} from 'react';
import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {SliderBox} from 'react-native-image-slider-box';
import {
	widthPercentageToDP,
	heightPercentageToDP,
} from 'react-native-responsive-screen';

const Home = ({
	navigation,
}) => {
	const [images, setImages] = useState([
		require('../images/architecture.jpg'),
		require('../images/people.jpg'),
		require('../images/desk.jpg'),
	]);

	return (
		<View style={styles.container}>
			<View>
				<Text style={styles.title}>Empresas</Text>
			</View>
			<View style={styles.background}>
				<SliderBox
					sliderBoxHeight={heightPercentageToDP(70)}
					images={images}
					autoplay={true}
				/>
			</View>
			<View style={styles.buttonArea}>
				<TouchableOpacity onPress={() => navigation.navigate('Login')}>
					<Text style={styles.button}>Login</Text>
				</TouchableOpacity>
			</View>
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
	},
	buttonArea: {
		position: 'relative',
		height: heightPercentageToDP(10),
		width: '100%',
		alignItems: 'center',
		justifyContent: 'center',
	},
	button: {
		backgroundColor: '#414d79',
		color: '#FFF',
		paddingVertical: 10,
		paddingHorizontal: 20,
		width: 100,
		textAlign: 'center',
	},
	background: {
		position: 'relative',
		height: heightPercentageToDP(70),
	},
	title: {
		position: 'relative',
		fontSize: 28,
		height: heightPercentageToDP(10),
		lineHeight: heightPercentageToDP(10),
		textAlign: 'justify',
		fontFamily: 'serif',
		color: '#414d79',
	},
});

export default Home;
