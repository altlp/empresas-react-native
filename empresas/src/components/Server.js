import React from 'react';

function fetchWithTimeout(url, options, timeout = 10000) {
	return Promise.race([
		fetch(url, options),
		new Promise((_, reject) =>
			setTimeout(() => reject(new Error('timeout')), timeout),
		),
	]);
}

export async function login(email, password) {
	return fetchWithTimeout(
		'https://empresas.ioasys.com.br/api/v1/users/auth/sign_in',
		{
			method: 'POST',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				email: email,
				password: password,
			}),
		},
		50000,
	);
}

export async function getEnterprises(access_token, client, ui) {
	return fetchWithTimeout(
		'https://empresas.ioasys.com.br/api/v1/enterprises',
		{
			method: 'GET',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
				'access-token': access_token,
				client: client,
				uid: ui,
			},
		},
		50000,
	)
		.then((res) => res)
		.then((res) => res.json())
		.then((json) => {
			return json.enterprises;
		});
}

export async function getEnterprisesFilter(
	access_token,
	client,
	ui,
	enterprise_types,
	name,
) {
	var url =
		'https://empresas.ioasys.com.br/api/v1/enterprises?name=' +
		encodeURIComponent(name);
	if (enterprise_types) {
		url += '&enterprise_types=' + encodeURIComponent(enterprise_types);
	}
	return fetchWithTimeout(
		url,
		{
			method: 'GET',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
				'access-token': access_token,
				client: client,
				uid: ui,
			},
		},
		50000,
	)
		.then((res) => res)
		.then((res) => res.json())
		.then((json) => {
			return json.enterprises;
		});
}

export async function getEnterpriseDetail(access_token, client, ui, id) {
	return fetchWithTimeout(
		'https://empresas.ioasys.com.br/api/v1/enterprises/' + id,
		{
			method: 'GET',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
				'access-token': access_token,
				client: client,
				uid: ui,
			},
		},
		50000,
	)
		.then((res) => res)
		.then((res) => res.json())
		.then((json) => {
			return json.enterprise;
		});
}