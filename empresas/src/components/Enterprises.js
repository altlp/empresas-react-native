import React, {useState, useEffect} from 'react';
import {
	StyleSheet,
	View,
	Text,
	TouchableOpacity,
	FlatList,
	// Image,
	Alert,
	Dimensions,
} from 'react-native';
import {
	widthPercentageToDP,
	heightPercentageToDP,
} from 'react-native-responsive-screen';

import Image from 'react-native-scalable-image';
import AsyncStorage from '@react-native-community/async-storage';
import RNPickerSelect from 'react-native-picker-select';
import Icon from 'react-native-vector-icons/MaterialIcons';
Icon.loadFont();

import CONSTANTS from './Constants';
import * as Server from './Server';
import {TextInput} from 'react-native-gesture-handler';

const tiposEmpresa = [
	{value: '1', label: 'Agro'},
	{value: '2', label: 'Aviation'},
	{value: '3', label: 'Biotech'},
	{value: '4', label: 'Eco'},
	{value: '5', label: 'Ecommerce'},
	{value: '6', label: 'Education'},
	{value: '7', label: 'Fashion'},
	{value: '8', label: 'Fintech'},
	{value: '9', label: 'Food'},
	{value: '10', label: 'Games'},
	{value: '11', label: 'Health'},
	{value: '12', label: 'IOT'},
	{value: '13', label: 'Logistics'},
	{value: '14', label: 'Media'},
	{value: '15', label: 'Mining'},
	{value: '16', label: 'Products'},
	{value: '17', label: 'Real Estate'},
	{value: '18', label: 'Service'},
	{value: '19', label: 'Smart City'},
	{value: '20', label: 'Social'},
	{value: '21', label: 'Software'},
	{value: '22', label: 'Technology'},
	{value: '23', label: 'Tourism'},
	{value: '24', label: 'Transport'},
];

const Enterprise = ({route, navigation}) => {
	const [enterprises, setEnterprises] = useState([]);
	const [enterpriseId, setEnterpriseId] = useState([]);
	const [enterpriseType, setEnterpriseType] = useState('');
	const [enterpriseName, setEnterpriseName] = useState('');
	const [showFilter, setShowFilter] = useState(false);
	useEffect(() => {
		async function prepare() {
			if (
				route.params != null &&
				(route.params.enterpriseType || route.params.enterpriseName)
			) {
				setEnterpriseName(route.params.enterpriseName);
				setEnterpriseType(route.params.enterpriseType);
				const result = await Server.getEnterprisesFilter(
					await AsyncStorage.getItem('access-token'),
					await AsyncStorage.getItem('client'),
					await AsyncStorage.getItem('uid'),
					route.params.enterpriseType,
					route.params.enterpriseName,
				);
				setEnterprises(result);
			} else {
				const result = await Server.getEnterprises(
					await AsyncStorage.getItem('access-token'),
					await AsyncStorage.getItem('client'),
					await AsyncStorage.getItem('uid'),
				);
				setEnterprises(result);
			}
		}
		prepare();
		if (enterprises == null) {
			Alert.alert(
				'Error',
				'You have been disconnected. Please login again.',
				[
					{
						text: 'Ok',
						onPress: () => navigation.navigate('Login'),
					},
				],
			);
		}
	}, []);

	const toggleFilter = () => {
		setShowFilter(!showFilter);
	};

	const search = () => {
		navigation.push('Enterprises', {
			enterpriseName: enterpriseName,
			enterpriseType: enterpriseType,
		});
	};

	const openMoreInfo = (id) => {
		setEnterpriseId(id);
		navigation.navigate('EnterpriseDetail', {enterpriseId: id});
	};

	const renderItem = ({item}) => {
		const image = CONSTANTS.BASE_URL + item.photo;
		return (
			<>
				<View style={styles.listItem}>
					<Text style={styles.itemTitle}>{item.enterprise_name}</Text>
					<Image
						width={Dimensions.get('window').width - 80}
						style={styles.itemImage}
						source={{uri: image}}
						resizeMode="contain"
					/>
					<TouchableOpacity
						style={styles.moreInfo}
						onPress={() => {
							openMoreInfo(item.id);
						}}>
						<Text style={styles.moreInfoText}>More info</Text>
					</TouchableOpacity>
				</View>
			</>
		);
	};

	return (
		<View style={styles.container}>
			<View style={styles.header}>
				<Text style={styles.title}>Empresas</Text>
				<Icon
					name="sort"
					size={30}
					color="#000"
					style={styles.icon}
					onPress={toggleFilter}
				/>
			</View>

			<View style={[styles.filter, showFilter && {display: 'flex'}]}>
				<View style={{width: '100%'}}>
					<RNPickerSelect
						useNativeAndroidPickerStyle={false}
						style={pickerSelectStyles}
						value={enterpriseType}
						onValueChange={(value) => setEnterpriseType(value)}
						items={tiposEmpresa}
					/>
				</View>
				<TextInput
					placeholder={'Search by name'}
					onChangeText={(value) => {
						setEnterpriseName(value);
					}}
					value={enterpriseName}
					style={styles.input}
					placeholderTextColor="#B2B2B2"
				/>
				<TouchableOpacity style={styles.iconButton} onPress={search}>
					<Icon
						name="search"
						size={30}
						color="#000"
						style={styles.icon}
					/>
				</TouchableOpacity>
			</View>

			<FlatList
				contentContainerStyle={styles.list}
				data={enterprises}
				renderItem={renderItem}
				keyExtractor={(item) => item.id.toString()}
			/>
		</View>
	);
};

const styles = StyleSheet.create({
	header: {
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		color: '#FFF',
		paddingHorizontal: 20,
		paddingVertical: 20,
	},
	filter: {
		flexDirection: 'row',
		flexWrap: 'wrap',
		justifyContent: 'center',
		alignItems: 'center',
		color: '#FFF',
		padding: 20,
		paddingTop: 0,
		display: 'none',
	},
	input: {
		flex: 5,
		color: '#FFF',
		borderColor: '#FFF',
		borderStyle: 'solid',
		borderWidth: 1,
		paddingHorizontal: 10,
	},
	icon: {
		color: '#FFF',
		textAlign: 'center',
	},
	iconButton: {
		flex: 1,
	},
	title: {
		flex: 4,
		position: 'relative',
		fontSize: 28,
		textAlign: 'left',
		fontFamily: 'serif',
		color: '#FFF',
	},
	container: {
		flex: 1,
		alignItems: 'center',
		backgroundColor: '#414d79',
	},
	list: {
		marginTop: 10,
		bottom: 10,
		paddingHorizontal: 20,
	},
	listItem: {
		backgroundColor: '#FFF',
		marginTop: 20,
		padding: 20,
	},
	itemTitle: {
		fontSize: 18,
		marginBottom: 15,
		color: '#000',
	},
	itemImage: {
		marginBottom: 10,
	},
	itemDescription: {
		fontSize: 12,
		lineHeight: 20,
	},
	moreInfo: {
		marginLeft: 'auto',
		width: 90,
		paddingHorizontal: 15,
		paddingVertical: 5,
		textAlign: 'center',
		backgroundColor: '#414d79',
	},
	moreInfoText: {
		textAlign: 'center',
		color: '#FFF',
	},
	background: {
		position: 'relative',
		height: heightPercentageToDP(70),
	},
});

const pickerSelectStyles = StyleSheet.create({
	inputIOS: {
		fontSize: 16,
		paddingVertical: 12,
		paddingHorizontal: 10,
		color: '#FFF',
		paddingRight: 30,
		borderColor: '#FFF',
		borderStyle: 'solid',
		borderWidth: 1,
		marginBottom: 5,
	},
	inputAndroid: {
		width: '100%',
		fontSize: 16,
		paddingHorizontal: 10,
		paddingVertical: 8,
		color: '#FFF',
		paddingRight: 30,
		borderColor: '#FFF',
		borderStyle: 'solid',
		borderWidth: 1,
		marginBottom: 5,
	},
});

export default Enterprise;
