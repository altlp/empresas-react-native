import React, {useState, useEffect} from 'react';
import {
	StyleSheet,
	View,
	Text,
	Alert,
	Dimensions,
	TouchableHighlight,
} from 'react-native';
import {
	widthPercentageToDP,
	heightPercentageToDP,
} from 'react-native-responsive-screen';

import Image from 'react-native-scalable-image';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/MaterialIcons';
Icon.loadFont();

import CONSTANTS from './Constants';
import * as Server from './Server';

const EnterpriseDetail = ({route, navigation}) => {
	const [enterpriseId, setEnterpriseId] = useState(route.params.enterpriseId);
	const [enterprise, setEnterprise] = useState(null);
	useEffect(() => {
		async function prepare() {
			const result = await Server.getEnterpriseDetail(
				await AsyncStorage.getItem('access-token'),
				await AsyncStorage.getItem('client'),
				await AsyncStorage.getItem('uid'),
				enterpriseId,
			);
			setEnterprise(result);
			if (result == null) {
				Alert.alert(
					'Error',
					'You have been disconnected. Please login again.',
					[
						{
							text: 'Ok',
							onPress: () => navigation.navigate('Login'),
						},
					],
				);
			}
		}
		prepare();
	}, []);

	return (
		<>
			{enterprise != null && (
				<View style={styles.container}>
					<View>
						<Image
							width={Dimensions.get('window').width}
							style={styles.itemImage}
							source={{
								uri: CONSTANTS.BASE_URL + enterprise.photo,
							}}
							resizeMode="contain"
						/>
					</View>

					<View
						style={[styles.listItem, {backgroundColor: '#414d79'}]}>
						<Icon
							name="work"
							size={20}
							color="#000"
							style={styles.icon}
						/>
						<Text style={[styles.itemTitle, styles.flexText]}>
							{enterprise.enterprise_name}
						</Text>
					</View>
					<View
						style={[styles.listItem, {backgroundColor: '#4D5D9C'}]}>
						<Icon
							name="edit"
							size={20}
							color="#000"
							style={styles.icon}
						/>
						<Text style={[styles.itemDescription, styles.flexText]}>
							{enterprise.description}
						</Text>
					</View>
					<View
						style={[styles.listItem, {backgroundColor: '#5769AF'}]}>
						<Icon
							name="flag"
							size={20}
							color="#000"
							style={styles.icon}
						/>
						<Text style={[styles.itemDescription, styles.flexText]}>
							{enterprise.country}
						</Text>
					</View>
					<View
						style={[styles.listItem, {backgroundColor: '#5E71C0'}]}>
						<Icon
							name="place"
							size={20}
							color="#000"
							style={styles.icon}
						/>
						<Text style={[styles.itemDescription, styles.flexText]}>
							{enterprise.city}
						</Text>
					</View>
				</View>
			)}
		</>
	);
};

const styles = StyleSheet.create({
	listItem: {
		backgroundColor: '#f5f5f5',
		padding: 20,
		width: '100%',
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		color: '#FFF',
	},
	icon: {
		flex: 1,
		color: '#FFF',
	},
	flexText: {
		flex: 4,
		color: '#FFF',
	},
	itemTitle: {
		fontSize: 18,
	},
	itemDescription: {
		fontSize: 12,
		lineHeight: 20,
		textAlign: 'left',
	},
	moreInfo: {
		marginLeft: 'auto',
		backgroundColor: '#414d79',
		width: 90,
		paddingHorizontal: 15,
		paddingVertical: 5,
		textAlign: 'center',
	},
	moreInfoText: {
		textAlign: 'center',
		color: '#FFF',
	},
	container: {
		flex: 1,
		alignItems: 'center',
	},
	buttonArea: {
		position: 'relative',
		height: heightPercentageToDP(10),
		width: '100%',
		alignItems: 'center',
		justifyContent: 'center',
	},
	button: {
		backgroundColor: '#414d79',
		color: '#FFF',
		paddingVertical: 10,
		paddingHorizontal: 20,
		width: 100,
		textAlign: 'center',
	},
	background: {
		position: 'relative',
		height: heightPercentageToDP(70),
	},
	title: {
		position: 'relative',
		fontSize: 28,
		height: heightPercentageToDP(10),
		lineHeight: heightPercentageToDP(10),
		textAlign: 'justify',
		fontFamily: 'serif',
	},
});

export default EnterpriseDetail;
