import React, {useState} from 'react';
import {
	StyleSheet,
	View,
	Text,
	TouchableOpacity,
	TextInput,
	Alert,
} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {
	widthPercentageToDP,
	heightPercentageToDP,
} from 'react-native-responsive-screen';
import * as Server from '../components/Server';
import AsyncStorage from '@react-native-community/async-storage';

const Login = ({navigation}) => {
	const [email, onChangeEmail] = useState('');
	const [password, onChangePassword] = useState('');
	const login = async () => {
		var header = await Server.login(email, password).then((result) => {
			return result.headers;
		});
		if (header.get('access-token') != null) {
			AsyncStorage.setItem('access-token', header.get('access-token'));
			AsyncStorage.setItem('client', header.get('client'));
			AsyncStorage.setItem('uid', header.get('uid'));
			navigation.navigate('Enterprises');
		} else {
			Alert.alert(
				'Invalid Credentials',
				'The email and password you entered did not match our records.',
			);
		}
	};

	return (
		<View style={styles.container}>
			<Text style={styles.title}>Empresas</Text>
			<TextInput
				placeholder="Email"
				onChangeText={(text) => onChangeEmail(text)}
				autoCapitalize="none"
				autoCompleteType="email"
				keyboardType="email-address"
				textContentType="emailAddress"
				value={email}
				style={styles.textInput}
			/>
			<TextInput
				placeholder="Password"
				onChangeText={(text) => onChangePassword(text)}
				autoCapitalize="none"
				autoCompleteType="password"
				secureTextEntry={true}
				value={password}
				style={styles.textInput}
			/>
			<View style={styles.buttonArea}>
				<TouchableOpacity onPress={login}>
					<Text style={styles.button}>Login</Text>
				</TouchableOpacity>
			</View>
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		width: '100%',
		paddingVertical: 10,
		paddingHorizontal: 20,
		// justifyContent: 'center',
		backgroundColor:'#414d79'
	},
	title: {
		position: 'relative',
		fontSize: 28,
		height: heightPercentageToDP(10),
		lineHeight: heightPercentageToDP(10),
		textAlign: 'justify',
		fontFamily: 'serif',
		marginBottom: 40,
		color: '#FFF',
	},
	textInput: {
		borderWidth: 2,
		borderColor: '#414d7960',
		borderRadius: 2,
		width: '100%',
		marginBottom: 20,
		paddingHorizontal: 10,
		color: '#000',
		backgroundColor:'#FFF'
	},
	buttonArea: {
		position: 'relative',
		width: '100%',
		alignItems: 'center',
	},
	button: {
		backgroundColor: '#FFF',
		color: '#414d79',
		paddingVertical: 10,
		paddingHorizontal: 20,
		width: 100,
		textAlign: 'center',
	},
});

export default Login;
