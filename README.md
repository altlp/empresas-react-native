# Api Empresas - React Native

### Como executar
* cd empresas && npm install
* react-native run-android

### Escolha das bibliotecas
* @react-native-community/async-storage - Utilizado para salvar dados de sessão local, é importante utilizar essa versão pois o async-storage do pacote react-native foi descontinuado.
* react-native-scalable-image - Utilizei pois essa biblioteca permite inserir imagens utilizando o 'height' como 'auto'.
* react-native-picker-select - Botão de select comum.
* react-native-gesture-handler - Input de texto comum.
* react-native-vector-icons/MaterialIcons - Icones do pacote material da Google
* react-native-responsive-screen - Utilizei o pacote por causa das suas funções de converter % em DP, algo que facilita no desenvolvimento de layouts responsivos.
* @react-navigation/native & @react-navigation/stack - Utilizado para definir as rotas das páginas.